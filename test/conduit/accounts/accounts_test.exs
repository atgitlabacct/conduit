defmodule Conduit.AccountsTest do
  import Conduit.Factory

  use Conduit.DataCase

  alias Conduit.Accounts
  alias Conduit.Accounts.Projections.User
  alias Conduit.Auth

  describe "register user" do
    @tag :integration
    test "should succeed with valid data" do
      assert {:ok, %User{} = user} = Accounts.register_user(build(:user))

      assert user.bio == nil
      assert user.email == "jake@jake.jake"
      assert user.image == nil
      assert user.username == "jake"
    end

    @tag :integrations
    test "shouild fail when identical username at same time and return error" do
      1..2
      |> Enum.map(fn _ ->
        Task.async(fn ->
          Accounts.register_user(build(:user))
        end)
      end)
      |> Enum.map(&Task.await/1)
    end

    @tag :integration
    test "should fail with invalid data and return error" do
      assert {:error, :validation_failure, errors} =
               Accounts.register_user(build(:user, username: ""))

      assert errors == %{username: ["can't be empty"]}
    end

    @tag :integration
    test "should fail when username already taken and return errro" do
      assert {:ok, %User{}} = Accounts.register_user(build(:user))

      assert {:error, :validation_failure, errors} =
               Accounts.register_user(build(:user, email: "new@new.com"))

      assert errors == %{username: ["has already been taken"]}
    end

    @tag :integration
    test "should fail when username format is invalid and return error" do
      assert {:error, :validation_failure, errors} =
               Accounts.register_user(build(:user, username: "j@ke"))

      assert errors == %{username: ["is invalid"]}
    end

    @tag :integration
    test "should convert username to lowercase" do
      assert {:ok, %User{} = user} =
               Accounts.register_user(build(:user, username: "JAKE"))

      assert user.username == "jake"
    end

    @tag :integration
    test "should fail when email address already taken and return error" do
      assert {:ok, %User{}} =
               Accounts.register_user(build(:user, username: "jake"))

      errors = %{email: ["has already been taken"]}

      assert {:error, :validation_failure, ^errors} =
               Accounts.register_user(build(:user, username: "jake2"))
    end

    @tag :integration
    test "should fail when registering identical email addresses at same time and return error" do
      1..2
      |> Enum.map(fn x ->
        Task.async(fn ->
          Accounts.register_user(build(:user, username: "user#{x}"))
        end)
      end)
      |> Enum.map(&Task.await/1)
    end

    @tag :integration
    test "should fail when email address format is invalid and return error" do
      errors = %{email: ["is invalid"]}

      assert {:error, :validation_failure, ^errors} =
               Accounts.register_user(build(:user, email: "invalid email"))
    end

    @tag :integration
    test "should convert email address to lowercase" do
      assert {:ok, %User{} = user} =
               Accounts.register_user(build(:user, email: "JAKE@JAKE.com"))

      assert user.email == "jake@jake.com"
    end

    @tag :integration
    test "should hash password" do
      assert {:ok, %User{} = user} = Accounts.register_user(build(:user))
      assert Auth.validate_password("jakejake", user.hashed_password)
    end
  end
end
