defmodule Conduit.BlogTest do
  use Conduit.DataCase

  alias Conduit.Blog
  alias Conduit.Blog.Projections.Article

  describe "list articles" do
    setup [:create_author, :publish_articles]

    @tag :integration
    test "should list articles by published date", %{
      articles: [article1, article2]
    } do
      assert {[article2, article1], 2} == Blog.list_articles(%{})
    end

    @tag :integration
    test "should limit articles", %{
      articles: [_article1, article2]
    } do
      assert {[article2], 2} == Blog.list_articles(%{limit: 1})
    end

    @tag :integration
    test "should paginate articles", %{
      articles: [article1, _article2]
    } do
      assert {[article1], 2} == Blog.list_articles(%{limit: 1, offset: 1})
    end

    @tag :integration
    test "should filter articles by author" do
      {:ok, author} = fixture(:author, username: "bob")
      {:ok, article} = fixture(:article, author: author, title: "A new article")

      assert {[article], 1} == Blog.list_articles(%{author: author.username})
    end

    @tag :integration
    test "should filter by tag returning only tagged articles", %{
      articles: [article1, _article2]
    } do
      assert {[article1], 1} == Blog.list_articles(%{tag: "believe"})
    end

    @tag :integration
    test "should filter by tag" do
      assert {[], 0} == Blog.list_articles(%{tag: "MISSED"})
    end
  end

  describe "publish article" do
    setup [:create_author]

    @tag :integration
    test "should succeed with valid data", %{author: author} do
      assert {:ok, %Article{} = article} =
               Blog.publish_article(author, build(:article))

      assert article.slug == "how-to-train-your-dragon"
      assert article.title == "How to train your dragon"
      assert article.description == "Ever wonder how?"
      assert article.body == "You have to believe"
      assert article.tags == ["dragons", "training"]
      assert article.author_username == "jake"
      assert article.author_bio == nil
      assert article.author_image == nil
    end

    @tag :integration
    test "should generate unique URL slug", %{author: author} do
      # First article with slug
      assert {:ok, %Article{} = article2} =
               Blog.publish_article(author, build(:article))

      assert article2.slug == "how-to-train-your-dragon"

      # Second article with slug incremented
      assert {:ok, %Article{} = article2} =
               Blog.publish_article(author, build(:article))

      assert article2.slug == "how-to-train-your-dragon-2"
    end
  end

  describe "find article by slug" do
    setup [:create_author, :publish_articles]

    test "returns the article", %{articles: [article1, _article2]} do
      assert article1 == Blog.article_by_slug!(article1.slug)
    end

    test "raises error when not found" do
      assert_raise(
        Ecto.NoResultsError,
        ~r/^expected at least one result but got none/,
        fn -> Blog.article_by_slug!("missing-slug") end
      )
    end
  end

  describe "list articles favorited by user" do
    setup [
      :create_author,
      :publish_articles,
      :favorite_article
    ]
  end
end
