defmodule ConduitWeb.UserControllerTest do
  use ConduitWeb.ConnCase
  use Conduit.DataCase

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "register user" do
    @tag :web
    test "should create an return user when data is valid", %{conn: conn} do
      conn = post(conn, user_path(conn, :create), user: build(:user))
      json = json_response(conn, 201)["data"]

      assert json == %{
               "bio" => nil,
               "email" => "jake@jake.jake",
               "image" => nil,
               "username" => "jake"
             }
    end

    test "should not create user and render errors when data is invalid", %{
      conn: conn
    } do
      conn =
        post(conn, user_path(conn, :create), user: build(:user, username: ""))

      assert json_response(conn, 422)["errors"] == %{
               "username" => ["can't be empty"]
             }
    end

    test "should not create user and render errors when username has been taken",
         %{conn: conn} do
      # register a user
      {:ok, _user} = fixture(:user)

      # attempt to register the same username
      conn =
        post(
          conn,
          user_path(conn, :create),
          user: build(:user, email: "jake2@jake.jake")
        )

      assert json_response(conn, 422)["errors"] == %{
               "username" => ["has already been taken"]
             }
    end
  end
end
