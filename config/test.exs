use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :conduit, ConduitWeb.Endpoint,
  http: [port: 4001],
  server: false

config :ex_unit, capture_log: true
# Print only warnings and errors during test
config :logger, level: :warn

config :conduit, Conduit.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "atpublicacct",
  password: "",
  database: "conduit_test",
  hostname: "localhost",
  pool_size: 1

# Event Store
config :eventstore, EventStore.Storage,
  serializer: Commanded.Serialization.JsonSerializer,
  username: "atpublicacct",
  password: "",
  database: "conduit_eventstore_test",
  hostname: "localhost",
  pool_size: 1

config :comeonin, :bcrypt_log_rounds, 1
