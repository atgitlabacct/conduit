# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :conduit, ecto_repos: [Conduit.Repo]

# Configures the endpoint
config :conduit, ConduitWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base:
    "02sYVpsz6HRIDMM+yYXHAavKDTWoW3W8mswrGpgO/K351iuoDwDL35H3VkODQIka",
  render_errors: [view: ConduitWeb.ErrorView, accepts: ~w(json)]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# EventStore
config :commanded, event_store_adapter: Commanded.EventStore.Adapters.EventStore

config :commanded_ecto_projections, repo: Conduit.Repo

config :vex,
  sources: [
    Conduit.Support.Validators,
    Conduit.Accounts.Validators,
    Conduit.Blog.Validators,
    Vex.Validators
  ]

config :guardian, Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: "Conduit",
  ttl: {30, :days},
  allowed_drift: 2000,
  verify_issuer: true,
  secret_key:
    "WzJBrSWO0IzEvtCNcy20Lopo79iGo4FHo9Wt8KwwUTWKWqiZ40RRF97gHO+u8A4k",
  serializer: Conduit.Auth.GuardianSerializer

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
