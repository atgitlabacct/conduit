defmodule ConduitWeb.Router do
  use ConduitWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
    plug(Guardian.Plug.VerifyHeader, realm: "Token")
    plug(Guardian.Plug.LoadResource)
  end

  pipeline :article do
    plug(ConduitWeb.Plugs.LoadArticleBySlug)
  end

  scope "/api", ConduitWeb do
    pipe_through(:api)

    post("/articles", ArticleController, :create)
    get("/articles", ArticleController, :index)
    get("/articles/:slug", ArticleController, :show)

    # Favorite and unfavorite article
    scope "/articles/:slug" do
      pipe_through(:article)

      post("/favorite", FavoriteArticleController, :create)

      delete(
        "/favorite",
        FavoriteArticleController,
        :delete,
        as: :unfavorite_article
      )
    end

    post("/users/login", SessionController, :create)
    post("/users", UserController, :create)
  end
end
