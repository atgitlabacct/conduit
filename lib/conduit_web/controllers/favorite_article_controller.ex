defmodule ConduitWeb.FavoriteArticleController do
  use ConduitWeb, :controller
  use Guardian.Phoenix.Controller

  plug(
    Guardian.Plug.EnsureAuthenticated,
    %{handler: ConduitWeb.ErrorHandler} when action in [:create]
  )

  action_fallback(ConduitWeb.FallbackController)

  def create(conn, params, _user, _claims) do
    # Should call out to the Bounded Context (Blog)

    # Router should dispatch Command to Blog aggregate
    # Can I do eventual consistency
    IO.puts("======= assigns =========")
    IO.inspect(conn.assigns[:article])
    IO.puts("======= assigns =========")

    Conduit.Router.dispatch()

    render("show.json", %{})
  end
end
