defmodule Conduit.Blog.Queries.ListArticles do
  @moduledoc """
  Query to encapsulate pagination (articles and total count)
  """
  import Ecto.Query

  alias Conduit.Blog.Projections.Article

  defmodule Options do
    @moduledoc """
    Contains the options for querying articles
    """
    defstruct limit: 20,
              offset: 0,
              author: nil,
              tag: nil

    use ExConstructor
  end

  def paginate(params, repo) do
    options = Options.new(params)

    articles =
      options
      |> query()
      |> entries(options)
      |> repo.all

    total_count =
      options
      |> query()
      |> count()
      |> repo.aggregate(:count, :uuid)

    {articles, total_count}
  end

  defp entries(query, %Options{limit: limit, offset: offset} = opts) do
    query
    |> order_by([a], desc: a.published_at)
    |> limit(^limit)
    |> offset(^offset)
  end

  defp count(query) do
    query
    |> select([:uuid])
  end

  defp query(options) do
    from(a in Article)
    |> filter_by_tag(options)
    |> filter_by_author(options)
  end

  defp filter_by_author(query, %Options{author: nil}), do: query

  defp filter_by_author(query, %Options{author: author}) do
    query
    |> where(author_username: ^author)
  end

  defp filter_by_tag(query, %Options{tag: nil}), do: query

  defp filter_by_tag(query, %Options{tag: tag}) do
    from(
      a in query,
      where: fragment("? @> ?", a.tags, [^tag])
    )
  end
end
