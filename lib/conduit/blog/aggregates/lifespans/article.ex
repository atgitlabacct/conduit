defmodule Conduit.Blog.Aggregates.Lifespans.Article do
  @behaviour Commanded.Aggregates.AggregateLifespan

  def after_event(_event), do: 60_000
end
