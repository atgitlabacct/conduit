defmodule Conduit.Accounts.Supervisor do
  @moduledoc """
  Supervisor to start and watch all Accounts context projectors

  Projectors are responsible for building up our Read model for the
  application.  These NEED to be running otherwise our read model would become
  stale.  This gives us a nice way of make sure we are EVENTUALLY up to date on
  the read side.  Also notes that we can rebuild the RDMS storage from
  rerunning all events
  """
  use Supervisor

  alias Conduit.Accounts

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    Supervisor.init(
      [
        Accounts.Projectors.User
      ],
      strategy: :one_for_one
    )
  end
end
