defmodule Conduit.Accounts.Events.UserRegistered do
  @moduledoc """
  Domain Event when a user is registered to the system.
  """
  @derive [Poison.Encoder]

  defstruct [
    :user_uuid,
    :username,
    :email,
    :hashed_password
  ]

  alias __MODULE__

  @type t :: %UserRegistered{}
end
