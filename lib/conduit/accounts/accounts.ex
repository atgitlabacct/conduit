defmodule Conduit.Accounts do
  @moduledoc """
  The boundry for the Accounts system.
  """

  alias Conduit.Accounts.Commands.RegisterUser
  alias Conduit.Accounts.Projections.User
  alias Conduit.Accounts.Queries.{UserByEmail, UserByUsername}
  alias Conduit.Repo
  alias Conduit.Router

  @doc """
  Register a new user

  Generates a RegisterUser Command and then has the Router dispatch the Command
  where it is then processed by the User Aggregate.

  Since we are using a Strong consistency model we can now wait for the Router
  to finish and have a gaurentee that the User will have have created the
  UserRegistered event and the read model updated.  This is why we can
  immediately get the projection.
  """
  def register_user(attrs \\ %{}) do
    uuid = UUID.uuid4()

    register_user =
      attrs
      |> RegisterUser.new()
      |> RegisterUser.assign_uuid(:user_uuid, uuid)
      |> RegisterUser.downcase_username()
      |> RegisterUser.downcase_email()
      |> RegisterUser.hash_password()

    with(:ok <- Router.dispatch(register_user, consistency: :strong)) do
      # Return projection due to consistency: :strong
      get(User, uuid)
    else
      reply -> reply
    end
  end

  @doc """
  Get an existing user by their username, or return `nil` if not registered
  """
  def user_by_username(username) do
    username
    |> String.downcase()
    |> UserByUsername.new()
    |> Repo.one()
  end

  @doc """
  Get an existing user by their email, or return `nil` if not registered
  """
  def user_by_email(email) do
    email
    |> String.downcase()
    |> UserByEmail.new()
    |> Repo.one()
  end

  @doc """
  Get a single user by their UUID
  """
  def user_by_uuid(uuid) when is_binary(uuid) do
    Repo.get(User, uuid)
  end

  # shared privates

  defp get(schema, uuid) do
    case Repo.get(schema, uuid) do
      nil -> {:error, :not_found}
      projection -> {:ok, projection}
    end
  end
end
