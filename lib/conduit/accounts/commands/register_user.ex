defmodule Conduit.Accounts.Commands.RegisterUser do
  @moduledoc """
  Command for registering a user.

  We are using a struct here to minimize any typos when issuing the Command RegisterUser.
  We could accidently do something like

  `
  %RegsiterUser{
    user: "jake",
    email: "jake@jake.com"
  }
  `

  and this would now throw an error.
  """

  defstruct user_uuid: "",
            username: "",
            email: "",
            password: "",
            hashed_password: ""

  alias __MODULE__
  alias Conduit.Auth

  @type t :: %RegisterUser{}

  use ExConstructor
  use Vex.Struct

  validates(:user_uuid, uuid: true)

  validates(
    :username,
    string: true,
    format: [
      with: ~r/^[a-z0-9]+$/,
      allow_nil: true,
      allow_blank: true,
      message: "is invalid"
    ],
    presence: [message: "can't be empty"],
    unique_username: true
  )

  validates(
    :email,
    presence: true,
    string: true,
    unique_email: true,
    format: [
      with: ~r/\S+@\S+\.\S+/,
      allow_nil: true,
      allow_blank: true,
      message: "is invalid"
    ]
  )

  validates(:hashed_password, presence: true, string: true)

  defimpl Conduit.Support.Middleware.Uniqueness.UniqueFields,
    for: Conduit.Accounts.Commands.RegisterUser do
    def unique(_command),
      do: [
        {:username, "has already been taken"},
        {:email, "has already been taken"}
      ]
  end

  def assign_uuid(%RegisterUser{} = register_user, identity, uuid),
    do: Map.put(register_user, identity, uuid)

  def downcase_username(%RegisterUser{username: uname} = register_user)
      when is_binary(uname),
      do: %{register_user | username: String.downcase(uname)}

  def downcase_email(%RegisterUser{email: email} = register_user)
      when is_binary(email),
      do: %{register_user | email: String.downcase(email)}

  def hash_password(%RegisterUser{password: password} = register_user) do
    %RegisterUser{
      register_user
      | password: nil,
        hashed_password: Auth.hash_password(password)
    }
  end
end
