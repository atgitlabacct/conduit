defmodule Conduit.Accounts.Aggregates.User do
  @moduledoc """
  Aggregate consistency boundry for User under the Accounts bounded context.

  This is designed to:
  1. Protect business invariants
  2. Be small
  3. Reference other Aggreates by identity only
  4. Update other aggregates using :strong or :eventual consistency
  """
  defstruct [:uuid, :username, :email, :hashed_password]

  alias Conduit.Accounts.Aggregates.User
  alias Conduit.Accounts.Commands.RegisterUser
  alias Conduit.Accounts.Events.UserRegistered

  @type t :: %User{}

  @doc """
  Register a new user

  Takes a command and returns zero, one or more domain events.  We are using
  the naming 'execute' so we don't have to create a CommandHandler and we can
  now displatch directly to the aggregate instead of intermediate
  CommandHandler -- less code to write.
  """
  @spec execute(User.t(), RegisterUser.t()) :: UserRegistered.t()
  def execute(%User{uuid: nil}, %RegisterUser{} = register) do
    %UserRegistered{
      user_uuid: register.user_uuid,
      username: register.username,
      email: register.email,
      hashed_password: register.hashed_password
    }
  end

  # state mutators

  @doc """
  Mutate the aggregate state by applying the UserRegistered event.
  """
  @spec apply(User.t(), UserRegistered.t()) :: User.t()
  def apply(%User{} = user, %UserRegistered{} = registered) do
    %User{
      user
      | uuid: registered.user_uuid,
        username: registered.username,
        email: registered.email,
        hashed_password: registered.hashed_password
    }
  end
end
