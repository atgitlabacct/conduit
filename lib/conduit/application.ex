defmodule Conduit.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      # Supervisors
      supervisor(Conduit.Repo, []),
      supervisor(Conduit.Accounts.Supervisor, []),
      supervisor(Conduit.Blog.Supervisor, []),
      supervisor(ConduitWeb.Endpoint, []),

      # Workers
      worker(Conduit.Support.Unique, [])
    ]

    opts = [strategy: :one_for_one, name: Conduit.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    ConduitWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
